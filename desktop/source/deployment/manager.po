# translation of manager.po to Arabic
# Ossama M. Khayat <okhayat@yahoo.com>, 2006, 2007.
# Abd Al-Rahman Hosny <abdohosny@gmail.com>, 2007.
# extracted from
msgid ""
msgstr ""
"Project-Id-Version: manager\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-10-08 22:43+0200\n"
"PO-Revision-Date: 2007-09-05 11:06+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"X-Accelerator-Marker: ~\n"

#: dp_manager.src#RID_STR_COPYING_PACKAGE.string.text
msgid "Copying: "
msgstr "الطباعة:"

#: dp_manager.src#RID_STR_ERROR_WHILE_ADDING.string.text
msgid "Error while adding: "
msgstr "خطأ أثناء الإضافة:"

#: dp_manager.src#RID_STR_ERROR_WHILE_REMOVING.string.text
msgid "Error while removing: "
msgstr "خطأ أثناء الإزالة:"

#: dp_manager.src#RID_STR_PACKAGE_ALREADY_ADDED.string.text
msgid "Extension has already been added: "
msgstr "قد أضيف امتداد بالفعل:"

#: dp_manager.src#RID_STR_NO_SUCH_PACKAGE.string.text
msgid "There is no such extension deployed: "
msgstr "لم ينتشر أيّ إمتداد:"

